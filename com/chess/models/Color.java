package com.chess.models;

public enum Color {
    WHITE,
    BLACK
}