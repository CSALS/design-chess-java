package com.chess.models;

public class Board {
    private Box board[][];

    public Board() {
        this.initializeBoard();
    }

    public initializeBoard() {
        this.board = new Box[8][8];
        //Initialize the board according to chess
        board[0][0] = new Box(new Position(0, 0), new Knight(WHITE));
        //...and so on
    }

    public Box getBox(Position position) {
        int x = position.getX();
        int y = position.getY();
        if(x < 0 || y < 0 || x >= 8 || y >= 8) {
            throw new Exception("Index out of bound exception")
        }
        return board[x][y];
    }
}