package com.chess.models;

public class Box {
    private Position position;
    private Piece piece; //referenece to piece present at this box

    public Box(Position position, Piece piece) throws IllegalArgumentException{
        if(!this.validatePosition(position)) {
            throw new IllegalArgumentException("Position isn't valid in this chess board");
        }
        this.setPosition(position)
        this.setPiece(piece);
    }

    public boolean validatePosition(Position position) {
        int x = position.getX();
        int y = position.getY();
        if(x < 0 || y < 0 || x >= 8 || y >= 8) {
            return false;
        }
        return true;
    }

    public Position getPosition() {
        return this.position;
    }

    public void setPosition(Piece piece) {
        this.piece = piece;
    }

    public Piece getPiece() {
        return this.piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }
}