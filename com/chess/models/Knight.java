package com.chess.models;

public class Knight extends Piece {
    public Knight(Color color) {
        super(color);
    }

    //Return True if knight can move from start to end in the chess board, else False
    @Override
    public boolean canMove(Board board, Box start, Box end) {
        if(!this.validateBox(start) || !this.validateBox(end)) {
            return false;
        }

        Piece startPiece = start.getPiece();
        Piece endPiece = end.getPiece();

        //Cant kill same color piece
        if(endPiece != null && endPiece.getColor() == startPiece.getColor()) {
            return false;
        }

        int x_diff = Math.abs(start.getPosition().getX() - end.getPosition().getX());
        int y_diff = Math.abs(start.getPosition().getY() - end.getPosition().getY());

        //Only L shaped moves are allowed. In one direction there will be 2 steps and in other there will be 1 step only.
        if(x_diff * y_diff == 2) {
            return true;
        } else {
            return false;
        }
    }
}