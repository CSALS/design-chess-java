package com.chess.models;

public abstract class Piece {
    private Color color;
    private boolean killed; //When a piece is killed instead of deleting it from memory we keep it since in chess we can recover some pieces

    public Piece(Color color) {
        this.color = color;
        this.setKilled(false);
    }

    public void setKilled(boolean killed) {
        this.killed = killed;
    }

    public boolean isKilled() {
        return this.killed;
    }

    public Color getColor() {
        return this.color;
    }

    /**
     * This function used to check if this piece can move from the start to end
     * @param board is the chess board currently being used
     * @param start is the source location on the board
     * @param end is the destination location on the board
     * @return True if the piece can move, else False
     */
    abstract public boolean canMove(Board board, Box start, Box end);
}