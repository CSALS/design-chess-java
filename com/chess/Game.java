package com.chess;

import com.chess.models.*; //importing all models


public class Game {
    private Board board;
    private Player white;
    private Player black;

    /**
     * Initialize the initial state of the chess game
     */
    Game() {
        this.board = new Board();
        this.white = new Player(WHITE);
        this.black = new Player(BLACK);
        this.board.initializeBoard();
    }

    public boolean makeMove(Player player) {
        try {
            //Take input from user regarding source and destination
            Position start = new Position(1, 1);
            Position end = new Position(2, 3);

            Box startBox = board[start.getX()][start.getY()];
            Box endBox = board[end.getX()][end.getY()];

            Piece startPiece = startBox.getPiece();
            Piece endPiece = endBox.getPiece();
            
            //Example = if a knight can move from start to end in the board
            if(startPiece.canMove(board, start, end)) {
                startBox.setPiece(null);

                if(endPiece != null) {
                    endPiece.setKilled(true);
                }

                endBox.setPiece(startPiece);
                return true;
            } else {
                return false;
            }
        } catch(Exception e) {
            System.out.println("Error = ", e);
            return false;
        }
    }

    public void startGame() {
        Player current = this.white;
        Player other = this.black;

        //Execute until game is completed
        while(1) {
            boolean isLegal = makeMove(current);
            if(isLegal) {
                System.out.println("Success move!!");
                swap(current, other);
            }
        }
    }

    public void swap(Player a, Player b) {
        Player temp = a;
        a = b;
        b = temp;
    }
}